# Qibullet Simulation

[API Docs](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos_pkg_qibullet_simulation/qibullet_simulation.html)

This package makes the qibullet simulation accessible via an aaambos module.

## Local Install of the repo
- `conda activate aaambos`
- `conda install conda-build`
- `pip install opencv-python-headless`
- `conda develop .`

##  Addtional Conda Environment Installation for QiBullet 
QiBullet only runs in Python 3.9 or lower but _aaambos_ is programmed for 3.10 and above. Therefore, we need to run the simulator in an additonal conda environment.
Execute the following shell commands:
- `conda create --name qibullet python=3.9 click`
- `conda activate qibullet`
- `conda install -c conda-forge pybullet`
- `pip install qibullet opencv-python object2urdf`

- execute the `first_start.py` script in the conda environment and accept the license with `y`.
  `python3 qibullet_simulation/first_start.py`

On a fresh Ubuntu installation, you might need to install cmake, jsonnet, and or gcc (`sudo apt install cmake`, `conda install -c conda-forge jsonnet`, `sudo apt install gcc`)

## Run with the example configs
- `conda activate aaambos`
- `aaambos run --run_config=qibullet_simulation/configs/run_config_pg.yml --arch_config=qibullet_simulation/configs/arch_config_qibullet_example.yml`

The `arch_config.yml` expects your conda installation to be `miniconda` located in the home directory. Otherwise you need to change that.

Sadly, the qibullet simulation is not a friend of dying via Control+C / Keyboard Interrupt. Close the window manually and eventually `pkill` the processes if they still appear in `top`. Or just restart the computer.
