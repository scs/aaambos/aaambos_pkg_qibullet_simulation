#!/bin/sh

# bash script that activates a conda environment and runs the simulator_py39.py
#
# first argument: path to conda.sh, e.g., ~/miniconda3/etc/profile.d/conda.sh
# second argument: the name of the conda environment

. $1
SCRIPTPATH=$(dirname "$0")
conda activate $2
python3 -u $SCRIPTPATH/simulator_py39.py --pattern "<<<!!<<<{}>>>!!>>>\n" --robot $3 --exchange_dir $4 --ct  --js --p  --fc
