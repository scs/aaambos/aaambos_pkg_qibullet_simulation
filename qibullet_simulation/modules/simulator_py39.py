#!/usr/bin/env python3

# python 3.9

import json
import os
import sys
import traceback
import signal
from datetime import datetime
from json import JSONDecodeError
from pathlib import Path
from threading import Thread
from typing import Callable

import click
import cv2
import pybullet as pb
from object2urdf import ObjectUrdfBuilder
from qibullet import SimulationManager, PepperVirtual, Camera, NaoVirtual


objects_path = Path(os.path.dirname(os.path.realpath(__file__)), "..",  "objects")


def createURDFObjects():
    builder = ObjectUrdfBuilder(str(objects_path), urdf_prototype='_prototype_face.urdf')
    file_name = objects_path / "facecube" / "cube.obj"
    builder.build_urdf(filename=str(file_name), force_overwrite=True, decompose_concave=True,force_decompose=False, center='mass')
    # file_name = objects_path / "house" / "low_poly_house.obj"
    # builder.build_urdf(filename=str(file_name), force_overwrite=True, decompose_concave=True,force_decompose=False, center='mass')


def spawnFaceCube():
    cube_start_pos = [1, 0, 1]
    cube_start_orientation = pb.getQuaternionFromAxisAngle([1, 0, 0], 0.9)
    plane_id = pb.loadURDF(str(objects_path / "objects.urdf"), basePosition=cube_start_pos, baseOrientation=cube_start_orientation, globalScaling=0.1)
    pb.setGravity(0, 0, 0, plane_id)
    texture_id = pb.loadTexture(str(objects_path / "image.png"))
    pb.changeVisualShape(plane_id, -1, textureUniqueId=texture_id)
    pb.setGravity(0, 0, 0, plane_id)
    return plane_id


class IOBasedSim:

    def __init__(self, msg_pattern, robot, exchange_dir, fps, camera, js, p, fc):
        os.setpgrp()
        # createURDFObjects()
        self.MSG_PATTERN: str = msg_pattern

        self.steps = 0
        self.image_freq = fps

        self.simulation_manager = SimulationManager()
        self.exchange_dir = exchange_dir

        # Launch a simulation instances, with using a graphical interface.
        # Please note that only one graphical interface can be launched at a time
        self.client_id = self.simulation_manager.launchSimulation(gui=True)

        self.debug_paras = {}
        if fc:
            face_cube = spawnFaceCube()
            para_y = pb.addUserDebugParameter("face_cube_param_y", -2, 2, 0)
            para_z = pb.addUserDebugParameter("face_cube_param_z", 0.5, 1.5, 1.0)
            self.debug_paras = {'face_cube_param_y': (para_y, face_cube), 'face_cube_param_z': (para_z, face_cube)}

        # cubeStartPos = [0, 0, -1]
        # planeId = pb.loadURDF(str(objects_path / "modules.urdf"), basePosition=cubeStartPos,
        #                      globalScaling=5.0)
        # pb.setGravity(0, 0, 0, planeId)

        # Selection of the robot type to spawn (True : Pepper, False : NAO)
        self.is_pepper_robot = robot == "pepper"
        self.cameras = {}
        self.camera_handles = []
        if self.is_pepper_robot:
            # Spawning a virtual Pepper robot, at the origin of the WORLD frame, and a
            # ground plane
            self.pepper = self.simulation_manager.spawnPepper(
                self.client_id,
                translation=[0, 0, 0],
                quaternion=[0, 0, 0, 1],
                spawn_ground_plane=True)
            self.robot = self.pepper

            if "cd" in camera:
                self.camera_handles.append(self.pepper.subscribeCamera(
                    PepperVirtual.ID_CAMERA_DEPTH,
                    resolution=Camera.K_QVGA,
                    fps=self.image_freq
                ))
                self.cameras["CameraDepth"] = PepperVirtual.ID_CAMERA_DEPTH
                self.send_msg("cameraImg", {"file": self.exchange_dir + "/CameraDepth.png"})

            self.pepper.goToPosture("Stand", 0.6)

        else:
            # Or a NAO robot, at a default position
            self.nao = self.simulation_manager.spawnNao(
                self.client_id,
                spawn_ground_plane=True)

            self.robot = self.nao
        # This snippet is a blocking call, just to keep the simulation opened

        if "ct" in camera:
            assert PepperVirtual.ID_CAMERA_TOP == NaoVirtual.ID_CAMERA_TOP
            self.camera_handles.append(self.robot.subscribeCamera(
                PepperVirtual.ID_CAMERA_TOP,
                resolution=Camera.K_QVGA,
                fps=self.image_freq
            ))
            self.cameras["CameraTop"] = PepperVirtual.ID_CAMERA_TOP
            self.send_msg("cameraImg", {"file": self.exchange_dir + "/CameraTop.png"})

        if "cb" in camera:
            assert PepperVirtual.ID_CAMERA_TOP == NaoVirtual.ID_CAMERA_TOP
            self.camera_handles.append(self.robot.subscribeCamera(
                PepperVirtual.ID_CAMERA_BOTTOM,
                resolution=Camera.K_QVGA,
                fps=self.image_freq
            ))
            self.cameras["CameraBottom"] = PepperVirtual.ID_CAMERA_BOTTOM
            self.send_msg("cameraImg", {"file": self.exchange_dir + "/CameraBottom.png"})

        self.js = js
        self.p = p

        self.threads: list[Thread] = []
        # Stop the simulation

        print("---")
        signal.signal(signal.SIGINT | signal.SIGTERM | signal.SIGKILL, self.handle_sigint)
        self.input_thread = Thread(target=self.receive_msgs)
        self.input_thread.daemon = True
        self.input_thread.start()
        sys.stdout = sys.__stdout__

    def run(self):
        self.send_msg("started", {"value": True})
        joints = list(self.robot.joint_dict.keys())
        while True:
            self.steps += 1
            # if self.steps == 4:
            #     print("-")
            #     self.pepper.setAngles(["HeadYaw", "HeadPitch"], [0.5, 0.3], [0.1, 0.1])
            #     print("-")
            #     self.send_msg("hello", "pasd")

            for name, camera in self.cameras.items():
                try:
                    if self.robot.camera_dict[camera].isActive():
                        img = self.robot.camera_dict[camera].getFrame()
                        assert img is not None
                        cv2.imwrite(self.exchange_dir + f"/{name}.png", img)
                except AssertionError:
                    print("Camera", name, "does not have an image.")

            if self.js:
                angles = self.robot.getAnglesPosition(joints)
                self.send_msg("jointAngles", {"time": datetime.now().isoformat(), "angles": dict(zip(joints, angles))})

            if self.p:
                x, y, theta = self.robot.getPosition()
                self.send_msg("position", {"time": datetime.now().isoformat(), "x": x, "y": y, "theta": theta})

            self.update_pb_position()

            # if self.threads:
            #     if not self.threads[0].is_alive():
            #         self.threads.pop(0)

    def send_msg(self, topic, msg):
        sys.stdout.write(json.dumps({"topic": topic, "msg": msg})+"\n")

    def receive_msgs(self):
        line_to_complete = ""
        for line in sys.stdin:
            try:
                line_to_complete += line
                msg = json.loads(line_to_complete)
                line_to_complete = ""
                assert "topic" in msg and "msg" in msg, "require topic and msg as keys in input string"
                self.msg_callback(msg["topic"], msg["msg"])
            except (KeyboardInterrupt, SystemExit):
                return
            except JSONDecodeError:
                print(line)
                pass  # wait for line to complete is parsable for multiline json
            except:
                sys.stderr.write(traceback.format_exc())

    def msg_callback(self, topic, msg):
        # print("<--", topic, msg)
        # sys.stdout.write("<--\n")
        if topic == "moveTo":
            self.do_async(self.move_to_pos, x=msg["x"], y=msg["y"], theta=msg["theta"])

    def do_async(self, method: Callable, **kwargs):
        thread = Thread(target=method, kwargs=kwargs)
        thread.daemon = True
        thread.start()
        self.threads.append(thread)

    def handle_sigint(self, sig, event):
        print("handle sigint in simulator")
        for handle in self.camera_handles:
            self.pepper.unsubscribeCamera(handle)
        self.simulation_manager.stopSimulation(self.client_id)
        os.killpg(0, signal.SIGINT)
        sys.exit(0)

    def move_to_pos(self, x, y, theta):
        # status
        try:
            self.pepper.moveTo(x, y, theta, frame=PepperVirtual.FRAME_ROBOT)
        except (KeyboardInterrupt, SystemExit):
            # Does not work but will exit anyway anytime after execution
            return

    def update_pb_position(self):
        if self.debug_paras:
            para_y, para_z, object_id = 0.0, 1.0, None
            fc_para_name_y = "face_cube_param_y"
            fc_para_name_z = "face_cube_param_z"
            if fc_para_name_y in self.debug_paras:
                para_y, object_id = self.debug_paras[fc_para_name_y]
            if fc_para_name_z in self.debug_paras:
                para_z, object_id = self.debug_paras[fc_para_name_z]
            if object_id is not None:
                pb.resetBasePositionAndOrientation(object_id, [1, pb.readUserDebugParameter(para_y),
                                                                     pb.readUserDebugParameter(para_z)],
                                                         pb.getQuaternionFromAxisAngle([1, 0, 0], 0.9))


@click.command()
@click.option("--pattern", default="{}\n", help="the pattern in that the msgs are directed to the stdout (using the python format option '{}' is replaces with the content)")
@click.option("--robot", default="pepper", help="which robot to spawn, 'pepper' or 'nao'")
@click.option("--exchange_dir", help="directory to exchange files")
@click.option("--fps", default=0.5, type=float, help="fps. Frames per second to record, cameras, joints, position. E.g, 0.5 => every 2 seconds")
@click.option("--ct", is_flag=True, help="activate top camera")
@click.option("--cb", is_flag=True, help="activate bottom camera")
@click.option("--cd", is_flag=True, help="activate depth camera (pepper)")
@click.option("--js", is_flag=True, help="activate joint states)")
@click.option("--p", is_flag=True, help="robot position)")
@click.option("--fc", is_flag=True, help="spawn face cube)")
def main(pattern, robot, exchange_dir, fps, ct, cb, cd, js, p, fc):
    print("--")
    try:
        camera = []
        if ct:
            camera.append("ct")
        if cb:
            camera.append("cb")
        if cd:
            camera.append("cd")
        sim = IOBasedSim(pattern, robot, exchange_dir, fps, camera, js, p, fc)
        sim.run()
    except (KeyboardInterrupt, SystemExit):
        sim.handle_sigint(1, 1)
        print(traceback.format_exc())
        sys.exit(0)


if __name__ == "__main__":
    main()
