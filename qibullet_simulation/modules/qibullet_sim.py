from __future__ import annotations

import asyncio
import json
import os
import signal
import sys
import time
from pathlib import Path
from typing import Type

import cv2
import matplotlib.pyplot as plt
from aaambos.core.communication.service import CommunicationService
from aaambos.core.supervision.run_time_manager import ControlMsg
from aaambos.std.extensions.pipe_subprocess.pipe_subprocess import PipeSubprocessExtensionFeature, \
    PipeSubprocessExtension
from attrs import define

from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.module.base import Module, ModuleInfo
from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity


class QiBulletSim(Module, ModuleInfo):
    config: QiBulletSimConfig

    def __init__(self, config: ModuleConfig, com, log, ext, subprocess: PipeSubprocessExtension, *args,
                 **kwargs):
        super().__init__(config, com, log, ext, *args, **kwargs)
        self.subprocess = subprocess
        self.steps = 1
        self.started = False
        self.exchange_dir = config.general_run_config.get_instance_dir() / config.exchange_dir
        if not os.path.exists(self.exchange_dir):
            os.makedirs(self.exchange_dir)

    @classmethod
    def provides_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {}

    @classmethod
    def requires_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {
            PipeSubprocessExtensionFeature.name: (PipeSubprocessExtensionFeature, SimpleFeatureNecessity.Required)
        }

    @classmethod
    def get_module_config_class(cls) -> Type[ModuleConfig]:
        return QiBulletSimConfig

    async def initialize(self):
        os.setpgrp()
        await self.subprocess.start_subprocess(["sh", self.config.shell_script, os.path.expanduser(self.config.conda_script), self.config.conda_env_name, self.config.robot, str(self.exchange_dir)], callback=self.sim_callback, converter=self.convert_line)

    @staticmethod
    def convert_line(line):
        line = json.loads(line)
        return line["topic"], line["msg"]

    async def sim_callback(self, line):
        topic, msg = line
        match topic:
            case "started":
                self.started = True
            case "jointAngles":
                self.log.info(f"Joint Angles: {msg['angles']}")
            case "position":
                self.log.info(f"Position: x={msg['x']}, y={msg['y']}, theta={msg['theta']}")
            case _:
                print("->", topic, msg)

    async def step(self):
        if not self.steps:
            await self.subprocess.msg_subprocess({"topic": "123", "msg": "1234"})
        if self.started:
            self.steps += 1
        if self.steps % 10 == 0:
            await self.subprocess.msg_subprocess({"topic": "moveTo", "msg": {"x": 0.5, "y": 0.5, "theta": 0.0}})
        elif self.steps % 5 == 0:
            await self.subprocess.msg_subprocess({"topic": "moveTo", "msg": {"x": -0.5, "y": -0.5, "theta": 0.0}})


    def terminate(self, control_msg: ControlMsg = None) -> int:
        exit_code = super().terminate()
        # self.subprocess.terminate_sub_process()
        os.killpg(0, signal.SIGINT)
        return exit_code


@define(kw_only=True)
class QiBulletSimConfig(ModuleConfig):
    conda_script: str
    conda_env_name: str = "qibullet"
    module_path: str = "qibullet_simulation.modules.qibullet_sim"
    module_info: Type[ModuleInfo] = QiBulletSim
    restart_after_failure: bool = True
    expected_start_up_time: float | int = 0
    shell_script: Path | str = Path(os.path.dirname(os.path.realpath(__file__)), "run_sim_39.sh")
    robot: str = "pepper"
    exchange_dir: str = "qibullet_exchange"


def provide_module():
    return QiBulletSim
