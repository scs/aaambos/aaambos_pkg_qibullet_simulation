"""

This is the documentation of the aaambos package QiBullet Simulation.
It contains of an aaambos module, owm run and arch configs.

# About the package
The package implements an aaambos module that starts a qibullet simulation able to simulate the movement and joints of
a pepper and nao robot.
The aaambos module is able to communicate with other aaambos module if it is equipped with the necessary communication
requirements.

The simulation needs to run with Python 3.9. Therefore, the module executes a shell script as a subprocess that executes
a python script with the simulator.
The aaambos module communicates with the script via stdin and stdout (pipes).
The module is located in the `qibullet_sim.py`. The script for the qibullet simulation is `simulator_py39.py`.

# Background / Literature
The qibullet simulation is based on the pybullet simulator which is the python wrapper/version of the bullet simulation.

The qibullet documentation is quite limited. Here someone can find some useful examples: [Github QiBullet Wiki - Tutorials](https://github.com/softbankrobotics-research/qibullet/wiki/Tutorials:-Virtual-Robot)
Or the examples in the repo: [Examples](https://github.com/softbankrobotics-research/qibullet/tree/master/examples).

# Usage / Examples
See for general module communication the docs or blog posts.

For the subcommand handling, the module uses the `aaambos.std.extensions.pipe_subprocess.PipeSubprocessExtension`.
For communicating with the subprocess/simulation just await the `msg_subprocess` method with a dict with the keys `topic` and `msg`.
```python
await self.subprocess.msg_subprocess({"topic": "moveTo", "msg": {"x": 0.5, "y": 0.5, "theta": 0.0}})
```
The messages must be json encodeable (Primitive types).

Received msgs from the simulation are handled by the `sim_callback` method. The topic here is just a str.
```python
async def sim_callback(self, line):
    topic, msg = line
    match topic:
        case "started":
            self.started = True
```

Sending from the simulation to the module is also quite easy. Just call the `send_msg` (not awaitable)
```python
self.send_msg("myTopicHere", {"my_payload": 1234})
```
Received msgs are already splitted into topic and msg:
```python
def msg_callback(self, topic, msg):
    if topic == "moveTo":
        ...
```

In general some methods of the qibullet simulation block the execution. I would recommend to run them in a single thread.
 If you also want to msg back to successfull execution this is also with this approach quite simple (just call it after the exectuion.)
"""